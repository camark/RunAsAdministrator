# RunAsAdministrator

一个用于简化右键菜单**以管理员身份运行**功能的辅助程序，使程序能直接双击向系统请求提权。

## 说明

参数说明

- `-env` 将此程序加入系统目录(`system32`)下，以在任意地方通过命令行执行 `asadmin app.exe args`
- `-run` 直接执行程序
- `-dir` 指定工作目录
- `-args` 配置 `-run` 一起使用，作为执行程序的参数，多个参数使用引号 `"` 包含

不传任何开关参数时，命令被识别为创建快捷方式，会自动在系统桌面上创建快捷方式(如果是快捷方式，直接替换原来的快捷方式)。也可以直接将程序拖放到 `asadmin.exe` 上以实现相同的效果。

> 注意：当目录路径包含空格或目标程序需要参数时，需要使用引号("")将其包围

## 示例

```shell
asadmin.exe -run C:\Windows\notepad.exe
asadmin.exe -run D:\test.txt
asadmin.exe -run "C:\Windows\System32\cmd.exe /C /Q"
```

> 在执行了 `asadmin.exe -env` 后，可以在系统任意位置执行 `asadmin app.exe args`
